export default {
    api: {
        hosts: {
            base: ''
        }
    },
    logging: {
        level: 'debug',
        redux_middleware: true
    },
    deployment: {
        host: 'http://local.getfluxt.com:3001',
        base_path: ''
    }
};
