export default {
  api: {
      hosts: {
          base: ''
      }
  },
  logging: {
      level: 'error',
      redux_middleware: false
  },
  deployment: {
      host: '',
      base_path: ''
  }
};
